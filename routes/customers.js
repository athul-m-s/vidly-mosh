const express = require("express");
const app = express();

const mongoose = require("mongoose");

const { Customer, validate } = require("./../models/customer");

const auth = require("../middleware/auth");

app.get("/", async (req, res) => {
  const customer = await Customer.find().sort("name");
  res.send(customer);
});

// ========================================================

app.post("/", auth, async (req, res) => {
  const { error } = validate(req.body);

  if (error) {
    res.status(400).send(error.details[0].message);
    return;
  }

  const customer = new Customer({
    name: req.body.name,
    phone: req.body.phone,
    isGold: req.body.isGold,
  });

  //add inside try catch , ex.message in catch
  await customer.save();

  res.send(customer);
});

// =========================================================

app.put("/:id", auth, async (req, res) => {
  const { error } = validate(req.body);

  if (error) {
    res.status(400).send(error.details[0].message);
    return;
  }

  const customer = await Customer.findByIdAndUpdate(
    req.params.id,
    {
      name: req.body.name,
      phone: req.body.phone,
      isGold: req.body.isGold,
    },
    { new: true }
  );

  if (!customer) {
    res.status(404).send("The customer with id not found");
    return;
  }

  res.send(customer);
});

// =========================================================

app.delete("/:id", auth, async (req, res) => {
  const customer = await Customer.findByIdAndRemove(req.params.id, {
    new: true,
  });

  if (!customer) {
    res.status(404).send("The customers with id not found");
    return;
  }

  res.send(customer);
});

// =========================================================

app.get("/:id", async (req, res) => {
  const customer = await Customer.findById(req.params.id);

  if (!customer) {
    res.status(404).send("The customer with id not found");
    return;
  } else {
    res.send(customer);
  }
});

// =========================================================

module.exports = app;

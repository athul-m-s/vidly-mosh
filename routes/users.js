const express = require("express");
const app = express();

const mongoose = require("mongoose");
const { User, validate } = require("./../models/user");

const _ = require("lodash");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const config = require("config");

const auth = require("../middleware/auth");

app.get("/me", auth, async (req, res) => {
  const user = await User.findById(req.user._id).select("-password");
  res.send(user);
});

// ========================================================

app.post("/", async (req, res) => {
  const { error } = validate(req.body);

  if (error) {
    res.status(400).send(error.details[0].message);
    return;
  }

  let user = await User.findOne({ email: req.body.email });

  if (user) {
    res.status(400).send("User already registered !!");
    return;
  }

  user = new User({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
  });

  // user = new User(_.pick(req.body, ["name", "email", "password"]));

  const salt = await bcrypt.genSalt(10);
  user.password = await bcrypt.hash(user.password, salt);

  //add inside try catch , ex.message in catch
  await user.save();

  //   res.send({
  //     name: user.name,
  //     email: user.email,
  //   });

  const token = user.generateAuthToken();

  res
    .header("x-auth-token", token)
    .send(_.pick(user, ["_id", "name", "email"]));
});

// ========================================================

module.exports = app;

const express = require("express");
const app = express();

const mongoose = require("mongoose");
const { Movie, validate } = require("./../models/movie");
const { Genre } = require("./../models/genre");

const auth = require("../middleware/auth");

app.get("/", async (req, res) => {
  const movies = await Movie.find().sort("name");
  res.send(movies);
});

// ========================================================

app.post("/", auth, async (req, res) => {
  const { error } = validate(req.body);

  if (error) {
    res.status(400).send(error.details[0].message);
    return;
  }

  const genre = await Genre.findById(req.body.genreId);
  let gid = genre._id;
  let gname = genre.name;

  if (!genre) {
    res.status(400).send("Invalid Genre.");
  }

  const movie = new Movie({
    title: req.body.title,
    genre: {
      id: gid,
      name: gname,
    },
    numberInStock: req.body.numberInStock,
    dailyRentalRate: req.body.dailyRentalRate,
  });

  await movie.save();

  res.send(movie);
});

// =========================================================

app.delete("/:id", auth, async (req, res) => {
  let movie;
  try {
    movie = await Movie.findByIdAndDelete(req.params.id);
  } catch (ex) {
    console.log("Invalid movie ID provided.", ex.message);
    return res
      .status(400)
      .send({ error: "Invalid movie ID provided.", message: ex.message });
  }

  if (!movie) return res.status(404).send("Such movie was not found.");
  res.send(movie);
});

module.exports = app;

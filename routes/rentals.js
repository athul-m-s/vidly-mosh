const express = require("express");
const app = express();
const { Rental, validate } = require("../models/rental");
const { Customer } = require("../models/customer");
const { Movie } = require("../models/movie");
const mongoose = require("mongoose");
const Fawn = require("fawn");

const auth = require("../middleware/auth");

Fawn.init(mongoose);

// =========================================================

app.get("/", async (req, res) => {
  const rentals = await Rental.find().sort("-dateOut");
  res.send(rentals);
});

// =========================================================

app.post("/", auth, async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const customer = await Customer.findById(req.body.customerId);
  if (!customer) return res.status(400).send("Invalid customer.");

  const movie = await Movie.findById(req.body.movieId);
  if (!movie) return res.status(400).send("Invalid movie.");

  if (movie.numberInStock === 0)
    return res.status(400).send("Movie not in stock.");

  let rental = new Rental({
    customer: {
      _id: customer.id,
      name: customer.name,
      phone: customer.phone,
    },
    movie: {
      _id: movie.id,
      title: movie.title,
      dailyRentalRate: movie.dailyRentalRate,
    },
  });

  // Fawn does a series of steps(Task) to edit a mongoDB database.
  // If an error occurs on any of the steps, the database is returned to its initial state.

  //   rental = await rental.save();
  //   movie.numberInStock--;
  //   movie.save();
  //   res.send(rental);

  try {
    new Fawn.Task()
      .save("rentals", rental)
      .update(
        "movies",
        { _id: movie._id },
        {
          $inc: { numberInStock: -1 },
        }
      )
      .run();

    res.send(rental);
  } catch (ex) {
    //internal server error
    res.status(500).send("Something went wrong");
  }
});

// === DELETE ===

app.delete("/:id", auth, async (req, res) => {
  let rental;
  try {
    rental = await Rental.findByIdAndDelete(req.params.id);
  } catch (ex) {
    console.log("Invalid rental ID provided.", ex.message);
    return res.status(400).send({
      error: "Invalid rental ID provided.",
      info: ex.message,
    });
  }

  if (!rental)
    return res.status(404).send({
      error: `Rental with ID: ${req.params.id} was not found.`,
    });

  res.send(rental);
});

module.exports = app;

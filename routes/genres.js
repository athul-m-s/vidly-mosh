const express = require("express");
const app = express();

const mongoose = require("mongoose");
const { Genre, validate } = require("./../models/genre");

const auth = require("../middleware/auth");
const admin = require("../middleware/admin");

// ========================================================

app.get("/", async (req, res) => {
  throw new Error("Error to fetch genres !!");
  const genres = await Genre.find().sort("name");
  res.send(genres);
});

// ========================================================

app.post("/", auth, async (req, res) => {
  const { error } = validate(req.body);

  if (error) {
    res.status(400).send(error.details[0].message);
    return;
  }

  const genre = new Genre({
    name: req.body.name,
  });

  //add inside try catch , ex.message in catch
  await genre.save();

  res.send(genre);
});

// =========================================================

app.put("/:id", auth, async (req, res) => {
  const { error } = validate(req.body);

  if (error) {
    res.status(400).send(error.details[0].message);
    return;
  }

  const genre = await Genre.findByIdAndUpdate(
    req.params.id,
    { name: req.body.name },
    { new: true }
  );

  if (!genre) {
    res.status(404).send("The genre with id not found");
    return;
  }

  res.send(genre);
});

// =========================================================

app.delete("/:id", [auth, admin], async (req, res) => {
  const genre = await Genre.findByIdAndRemove(req.params.id, { new: true });

  if (!genre) {
    res.status(404).send("The genre with id not found");
    return;
  }

  res.send(genre);
});

// =========================================================

app.get("/:id", async (req, res) => {
  const genre = await Genre.findById(req.params.id);

  if (!genre) {
    res.status(404).send("The genre with id not found");
    return;
  } else {
    res.send(genre);
  }
});

// =========================================================

module.exports = app;

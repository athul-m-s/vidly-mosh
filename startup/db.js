const mongoose = require("mongoose");
const winston = require("winston");

module.exports = function () {
  //connecting to mongodb : database initialisation
  mongoose
    .connect("mongodb://localhost/vidly", {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useCreateIndex: true,
    })
    .then(() => winston.info("connected to MongoDB"))
    .catch((err) => console.log("error in connection with mongodb", err));

  mongoose.set("useFindAndModify", false);
};

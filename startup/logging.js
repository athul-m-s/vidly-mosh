const winston = require("winston");
require("winston-mongodb");
require("express-async-errors");

module.exports = function () {
  //handling and logging errors
  process.on("uncaughtException", (ex) => {
    winston.error(ex.message);
    process.exit(1);
  });

  process.on("unhandledRejection", (ex) => {
    winston.error(ex.message);
    process.exit(1);
  });

  winston.add(new winston.transports.Console());
  winston.add(new winston.transports.File({ filename: "logfile.log" }));
  winston.add(
    new winston.transports.MongoDB({
      level: "error",
      db: "mongodb://localhost/vidly",
      options: { useUnifiedTopology: true },
      metaKey: "meta",
    })
  );

  // throw new Error("startup errors !");
};

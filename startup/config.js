const config = require("config");

module.exports = function () {
  // set vidly_jwtPrivateKey=mySecurekey;
  if (!config.get("jwtPrivateKey")) {
    throw new Error("FATAL_ERROR:jwtPrivateKey NOT DEFINED");
    process.exit(1);
  }
};

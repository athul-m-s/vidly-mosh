const express = require("express");
const app = express();

const winston = require("winston");
require("winston-mongodb");

//add logging at first
require("./startup/logging")();
require("./startup/routes")(app);
require("./startup/db")();
require("./startup/config")();
require("./startup/validation")();

const port = process.env.PORT || 3000;

app.listen(port, winston.info("listening to port 3000"));
